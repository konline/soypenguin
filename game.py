#!/usr/bin/env python3

import soy
from model import Model
from time import sleep


lvlison = getLevel() #this will get the level from the map, once that is set up

map = soy.textures.Texture()
map.channels = 1
map.size = soy.atoms.Size((26, 26, 16))
#set up size of the landscape

mat = soy.materials.Colored('green')
#set up the colored mat

sce = soy.scenes.Landscape(map, mat, 1, soy.atoms.Size((32, 8, 32)), soy.atoms.Position((0,0,0)))
#set up the scene

#spawn area not taken into account yet
#an array of points will be held, corresponding to different levels
#each level will have its own spawn point, which will also be handled by the lvlison if statement
sce['penguin'] = Model('models/penguin_on_stand.obj')
sce['penguin'].position = soy.atoms.Position((0,3,0))
sce['penguin'].rotation = soy.atoms.Rotation((30, 0, 0))

#sce['terrain'] = Model('level/basic_level_1.obj')

#if lvlison == 1 :
#	set terrain 1 up
#elif lvlison == 2 :
#	set terrain 2 up
#etc

sce['cam'] = soy.bodies.Camera(soy.atoms.Position((0,30,30)))
sce['cam'].rotation = soy.atoms.Rotation((0, 0, 0.7))
sce['light'] = soy.bodies.Light(soy.atoms.Position((-30, 40, 60)))
#camera's movement will eventually smoothly follow the penguin
#from closer range than it is set up now
#around the map, as he moves. This will be achieved by adding a small force to the camera
#and adding equal negative force after it has moved the desired amount of space

client = soy.Client()
client.window.append(soy.widgets.Projector(sce['cam']))

key = soy.controllers.Keyboard()
#eventually, this read in keypresses from the keyboard once I am able to use the controllers

#if key.key_press == 'Up'
	#if key.key_down == 'Right/Left' and penguin.isFacing == 'Right/Left'(if the rotation value of x is 30
	# then it's facing right)
	#this will act as a sort of stabilizer as well, so the penguin does not fall over
		#penguin.addForce(-50/50,50,0)
	#elif
		#penguin.addForce(-50/50,50,0)
		#rotate penguin
	#elif	
		#penguin(have to streamline code a bit).addForce

#

if __name__ == '__main__' :
	while client.window :
		sleep(.1)

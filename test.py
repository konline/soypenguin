#!/usr/bin/env python3

import soy
from model import Model
from time import sleep

map = soy.textures.Texture()
map.channels = 1
map.size = soy.atoms.Size((16, 16, 16))

mat = soy.materials.Colored('green')

sce = soy.scenes.Landscape(map, mat, 1, soy.atoms.Size((32, 8, 32)), soy.atoms.Position((0,0,0)))

sce['penguin'] = Model('models/penguin_on_stand.obj')
#sce['penguin'].position = soy.atoms.Position((0,3,0))
#sce['penguin'].rotation = soy.atoms.Rotation((30, 0, 0))

sce['terrain'] = Model('level/basic_level_1.obj')
#sce['terrain'].position = soy.atoms.Position((0,3,0))
#sce['terrain'].rotation = soy.atoms.Rotation((30, 0, 0))


sce['cam'] = soy.bodies.Camera(soy.atoms.Position((0,30,30)))
sce['cam'].rotation = soy.atoms.Rotation((0, 0, 0.7))
sce['light'] = soy.bodies.Light(soy.atoms.Position((-30, 40, 60)))

client = soy.Client()
client.window.append(soy.widgets.Projector(sce['cam']))

key = soy.controllers.Keyboard()

if __name__ == '__main__' :
	while client.window :
		sleep(.1)
